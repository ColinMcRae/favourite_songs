require 'rails_helper'

describe Song do
  let(:user) { create :user }
  let(:list) { create :list, user_id: user.id }

  context 'valid' do
    it('with all fields') { expect(build(:song, list_id: list.id)).to be_valid }
    it('without url') { expect(build(:song, list_id: list.id, url: nil)).to be_valid }
    it('without genre') { expect(build(:song, list_id: list.id, genre: nil)).to be_valid }
  end

  context 'not valid' do
    it('without list') { expect(build(:song)).to_not be_valid }
    it('without name') { expect(build(:song, list_id: list.id, name: nil)).to_not be_valid }
    it('without author') { expect(build(:song, list_id: list.id, author: nil)).to_not be_valid }
  end
end
