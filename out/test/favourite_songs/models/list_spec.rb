require 'rails_helper'

describe List do
  let(:user) { create :user }

  context 'valid' do
    it('with all fields') { expect(build(:list, user_id: user.id)).to be_valid }
    it('without description') { expect(build(:list, user_id: user.id, description: nil)).to be_valid }
    it('without share') { expect(build(:list, user_id: user.id, share: nil)).to be_valid }
  end

  context 'not valid' do
    it('without user') { expect(build(:list)).to_not be_valid }
    it('without name') { expect(build(:list, user_id: user.id, name: nil)).to_not be_valid }
  end
end
