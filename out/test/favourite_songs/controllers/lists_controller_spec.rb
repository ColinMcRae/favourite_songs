require 'rails_helper'

context ListsController, type: :controller do
  let(:user) { create :user }
  let(:list) { create :list, user_id: user.id }
  let(:song1) { create :song, list_id: list.id }
  let(:song2) { create :song, list_id: list.id }
  let(:list_params) { { list: { name: 'listname', user_id: user.id } } }

  describe 'unsigned user' do
    context '#index' do
      before { get :index }
      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context '#create' do
      before { post :create, params: list_params }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context '#show' do
      before { get :show, params: { id: list.id } }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end

  describe 'signed in user' do
    let(:user) { create :user }
    before do
      sign_in user
    end

    context '#index' do
      before { get :index }
      it { expect(response).to have_http_status(:success) }
      it { expect(assigns(:lists)).to eq([list]) }
      it { expect(assigns(:new_list)).to be_new_record }
    end

    context '#create' do
      context '#create' do
        it { expect(post(:create, params: list_params)).to have_http_status(:redirect) }
        it { expect { post :create, params: list_params }.to change(List, :count).by(1) }
      end
    end

    context '#show' do
      before { get :show, params: { id: list.id } }
      it { expect(response).to have_http_status(:success) }
      it { expect(assigns(:list)).to eq(list) }
      it { expect(assigns(:new_song)).to be_new_record }
      it { expect(assigns(:songs)).to eq([song1, song2]) }
    end

    context '#update' do
      let(:edit_list_params){ { id: list.id, name: 'newname' } }

      before{ patch :update,edit_list_params }

      it { expect(list.reload.name).to eq(edit_list_params[:name]) }
    end
  end
end
