require 'rails_helper'

context DashboardController do
  describe '#index' do
    context 'unsigned' do
      before { get :index }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context 'signed in' do
      let(:user) { create :user }
      before do
        sign_in user
        get :index
      end

      it { expect(response).to have_http_status(:success) }

      context 'load lists'

      context 'load statistics'
    end
  end
end
