class CreateSongs < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.integer :list_id, null: false
      t.string :url
      t.string :author, null: false
      t.string :name, null: false
      t.string :genre

      t.timestamps
    end
  end
end
