class CreateListsShare < ActiveRecord::Migration[5.0]
  def change
    create_table :lists_shares do |t|
      t.integer :user_id, null:false
      t.integer :list_id, null:false
    end
  end
end
