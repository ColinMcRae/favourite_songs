class CreateSongsStatistics < ActiveRecord::Migration[5.0]
  def change
    create_table :songs_statistics do |t|
      t.integer :count, default: 0
      t.integer :song_id, null: false
      t.timestamps
    end
  end
end
