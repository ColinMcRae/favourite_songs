class ChangeListShareType < ActiveRecord::Migration[5.0]
  def up
    rename_column :lists, :share, :is_public
    change_column :lists, :is_public, 'boolean USING CAST(is_public AS boolean)'
  end

  def down
    change_column :lists, :is_public, 'varchar USING CAST(is_public AS varchar)'
    rename_column :lists, :is_public, :share
  end
end
