class ListsQuery < ApplicationQuery
  def all(user_id)
    shared_lists = ListsShare.where(user_id: user_id).map(&:list_id)
    default_relation.includes(:user)
                    .where('user_id = :user_id OR is_public = true OR id IN (:ids)',
                           user_id: user_id,
                           ids: shared_lists)
  end

  private

  def default_relation
    List.all
  end
end
