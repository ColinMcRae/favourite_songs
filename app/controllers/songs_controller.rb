class SongsController < ApplicationController
  before_action :load_song, only: %i[update destroy]

  def create
    result = Songs::Create.call(params: song_params)

    if result.error.present?
      flash[:danger] = result.error
    else
      flash[:success] = t(:song_added_successfully)
    end

    redirect_to list_path(result.song.list_id)
  end

  def update
    @song.update(song_params)
  end

  def destroy
    @song.destroy
  end

  private

  def load_song
    @song = Song.find(params[:id])
  end

  def song_params
    params.require(:song).permit(:url, :author, :name, :genre, :list_id)
  end
end
