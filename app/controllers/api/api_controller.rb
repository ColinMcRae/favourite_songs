module Api
  class ApiController < ActionController::API
    include Pundit
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    respond_to :json
    before_action :authenticate_by_token

    private

    def authenticate_by_token
      user = User.where.not(remember_token: nil)
                 .find_by(remember_token: params[:auth_token] || request.headers['X-AUTH-TOKEN'])
      if user
        sign_in(user)
      else
        render json: { errors: 'Access denied' }, status: :unauthorized
      end
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: { error: e.message }, status: :not_found
    end

    def user_not_authorized
      render json: { error: 'You are not authorized to perform this action.' }, status: :not_authorized
    end
  end
end
