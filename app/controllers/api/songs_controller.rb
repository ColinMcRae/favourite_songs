module Api
  class SongsController < Api::ApiController
    before_action :load_song, only: %i[update destroy]

    def create
      result = Songs::Create.call(params: song_params)

      responce = if result.error.present?
                   { status: 'failure', errors: result.error }
                 else
                   { status: 'success', song: result.song }
                 end

      render json: responce
    end

    def update
      result = if @song.update(song_params)
                 { status: 'success', list: @song }
               else
                 { status: 'failure', errors: @song.errors.messages }
               end
      render json: result
    end

    def destroy
      @song.destroy
      render json: { status: 'success', deleted_id: @song.id }
    end

    private

    def load_song
      @song = Song.find(params[:id])
    end

    def song_params
      params.require(:song).permit(:url, :author, :name, :genre, :list_id)
    end
  end
end
