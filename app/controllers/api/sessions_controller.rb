module Api
  class SessionsController < Devise::SessionsController
    before_action :authenticate_user!, except: [:create]
    before_action :ensure_params_exist, except: [:destroy]
    respond_to :json

    def create
      resource = User.find_for_database_authentication(email: params[:user_login][:email])
      return invalid_login_attempt unless resource

      if resource.valid_password?(params[:user_login][:password])
        sign_in(:user, resource)
        resource.remember_me!

        render json: { auth_token: resource.remember_token, email: resource.email }, status: :ok
        return
      end
      invalid_login_attempt
    end

    def destroy
      resource = User.find_by(remember_token: params[:auth_token] || request.headers['X-AUTH-TOKEN'])
      resource.forget_me!
      sign_out(resource)
      render json: {}.to_json, status: :ok
    end

    protected

    def ensure_params_exist
      return unless params[:user_login].blank?
      render json: { message: 'missing user_login parameter' }, status: 422
    end

    def invalid_login_attempt
      render json: { message: 'Error with your login or password' }, status: 401
    end
  end
end
