module Api
  class ListsController < Api::ApiController
    before_action :load_list, only: %i[edit show update destroy share]
    before_action :load_shares, only: %i[show share]

    def index
      lists = List.where(user_id: current_user.id)
      render json: { lists: lists, user_id: current_user.id }
    end

    def create
      result = Lists::Create.call(params: list_params, user_id: current_user.id)

      responce = if result.error.present?
                   { status: 'failure', errors: result.error }
                 else
                   { status: 'success', list: result.list }
                 end
      render json: responce
    end

    def show
      songs = @list.songs
      users_ids = User.all.ids
      render json: { list: @list, songs: songs, users_ids: users_ids, user_id: current_user.id }
    end

    def update
      result = if @list.update(list_params)
                 { status: 'success', list: @list }
               else
                 { status: 'failure', errors: @list.errors.messages }
               end
      render json: result
    end

    def destroy
      @list.destroy
      render json: { status: 'success', deleted_id: @list.id }
    end

    def share
      result = Lists::Share.call(list_id: params[:id], user_ids: share_params.keys, current_shares: @lists_shares)
      render json: { success: result.success }
    end

    private

    def load_list
      @list = List.find(params[:id])
      authorize @list
    end

    def load_shares
      @lists_shares = ListsShare.joins(:list).where(lists: { id: @list.id })
    end

    def list_params
      params.require(:list).permit(:id, :name, :description, :is_public)
    end
  end
end
