module Api
  class DashboardController < Api::ApiController
    def index
      @lists = ListsQuery.new.all(current_user.id)
      render json: { lists: @lists }
    end
  end
end
