class ListsController < ApplicationController
  before_action :load_list, only: %i[edit show update destroy share]
  before_action :load_shares, only: %i[show share]

  def index
    @lists = List.where(user_id: current_user.id).paginate(page: params[:page], per_page: 10)
    @new_list = List.new(user_id: current_user.id)
  end

  def create
    result = Lists::Create.call(params: list_params, user_id: current_user.id)

    if result.error.present?
      flash[:danger] = result.error
    else
      flash[:success] = t(:list_created_successfully)
    end

    redirect_to my_lists_path
  end

  def show
    @songs = @list.songs.paginate(page: params[:page], per_page: 10)
    @new_song = Song.new(list_id: @list.id)
    @users = User.all
  end

  def update
    @list.update(update_params)
    redirect_to list_path @list
  end

  def destroy
    @list.destroy
    redirect_to my_lists_path
  end

  def share
    Lists::Share.call(list_id: params[:id], user_ids: share_params.keys, current_shares: @lists_shares)
    redirect_to list_path @list
  end

  private

  def update_params
    params.require(:list).permit(:id, :name, :description, :is_public)
  end

  def list_params
    params.require(:list).permit(:name, :description)
  end

  def share_params
    params[:share].try(:permit!) || {}
  end

  def load_list
    @list = List.find(params[:id])
    authorize @list
  end

  def load_shares
    @lists_shares = ListsShare.joins(:list).where(lists: { id: @list.id })
  end
end
