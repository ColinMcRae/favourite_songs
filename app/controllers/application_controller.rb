class ApplicationController < ActionController::Base
  protect_from_forgery
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    user_attrs = %i[username email password password_confirmation]
    devise_parameter_sanitizer.permit :sign_up, keys: user_attrs
    devise_parameter_sanitizer.permit :account_update, keys: user_attrs << :info
    devise_parameter_sanitizer.permit :sign_in, keys: %i[email password]
  end

  private

  def user_not_authorized
    flash[:danger] = 'You are not authorized to perform this action.'
    redirect_to(request.referrer || root_path)
  end
end
