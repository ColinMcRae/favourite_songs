class SongsStatisticController < ApplicationController
  skip_before_action :authenticate_user!, only: [:collect]

  def collect
    respond_to :js
    SongsStatistic.collect(song_id)
    head :ok
  end

  private

  def song_id
    params.require(:song_id)
  end
end
