class DashboardController < ApplicationController
  def index
    @lists = ListsQuery.new.all(current_user.id).paginate(page: params[:page], per_page: 10)
    @stat = SongsStatistic.top_genre.to_json
  end
end
