stats = $('.js-stats-container').data('stat')
genres = []
clicks = []
$.each stats, (genre, count) ->
  genres.push genre
  clicks.push count
  return
chart = Highcharts.chart('container',
  title: text: 'Top genres per month'
  subtitle: text: ''
  xAxis: categories: genres
  series: [ {
    type: 'column'
    colorByPoint: true
    data: clicks
    showInLegend: false
  } ])
$('#plain').click ->
  chart.update
    chart:
      inverted: false
      polar: false
    subtitle: text: 'Plain'
  return
$('#inverted').click ->
  chart.update
    chart:
      inverted: true
      polar: false
    subtitle: text: 'Inverted'
  return
$('#polar').click ->
  chart.update
    chart:
      inverted: false
      polar: true
    subtitle: text: 'Polar'
  return