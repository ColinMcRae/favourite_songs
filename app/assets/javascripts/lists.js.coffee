forms = ['.js-share-form', '.js-new-song-form', '.js-edit-list-form']
hide_forms = () ->
  $.each forms, (i, form) ->
    $(form).hide()
    return

show_form = (form) ->
  if $(form).css('display') == 'block'
    hide_forms()
  else
    hide_forms()
    $(form).show()
  return

$('.js-hide-form').click ->
  hide_forms()
  return

$('.js-show-song-form-btn').click ->
  show_form('.js-new-song-form')
  return

$('.js-show-edit-list-form-btn').click ->
  show_form('.js-edit-list-form')
  return

$('.js-share-list-btn').click ->
  show_form('.js-share-form')
  return

$('.js-song-url').click ->
  if $(this).data('link') != ''
    console.log $(this).data('link')
    window.open($(this).data('link'), '_blank')
    $.post( "/collect_click", { song_id: $(this).data('song') });
  return