class SongPolicy < ApplicationPolicy
  def create?
    true
  end

  def update?
    song.list.user == user
  end

  def destroy?
    update?
  end

  private

  def song
    record
  end
end
