class ListPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    list.user_id == user.id || list.is_public || user.lists_shares.pluck(:list_id).include?(list.id)
  end

  def update?
    list.user_id == user.id
  end

  def destroy?
    update?
  end

  def share?
    update?
  end

  private

  def list
    record
  end
end
