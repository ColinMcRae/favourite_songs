module Lists
  class Create
    include Interactor

    delegate :params, :user_id, :list, to: :context

    def call
      context.list = List.create(params.merge(user_id: user_id))
      return if list.errors.empty?

      errors_list = list.errors.messages.inject([]) { |str, error| str << error.join(' ') }.join(',')
      context.error = 'Error creating list:' + errors_list
      context.fail!
    end
  end
end
