module Lists
  class Share
    include Interactor

    delegate :list_id, :user_ids, :current_shares, to: :context

    def call
      values = user_ids.inject([]) { |vals, u_id| vals << [u_id.to_i, list_id] }
      old_shares = current_shares.map { |share| [share.user_id, share.list_id] }

      new_shares = values - old_shares
      unshares = old_shares - values

      if unshares.present?
        ListsShare.where(user_id: unshares.map { |u| u[0] }, list_id: unshares.map { |u| u[1] }).delete_all
      end

      if new_shares.present?
        values = new_shares.inject([]) { |vals, share| vals << "(#{share[0]}, #{share[1]})" }.join(',')
        # TODO: - query
        sql = 'INSERT INTO lists_shares (user_id, list_id) VALUES' + values
        ActiveRecord::Base.connection.execute(sql)
      end
    end
  end
end
