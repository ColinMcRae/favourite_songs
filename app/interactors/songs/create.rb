module Songs
  class Create
    include Interactor

    delegate :params, :song, to: :context

    def call
      context.song = Song.create(params)
      return if song.errors.empty?
      errors_list = song.errors.messages.inject([]) { |str, error| str << error.join(' ') }.join(',')
      context.error = 'Song was not added:' + errors_list
      context.fail!
    end
  end
end
