class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :validatable, :rememberable
  validates_presence_of :username
  has_many :lists
  has_many :lists_shares
  has_many :shared_lists, through: :lists_shares, source: :list
end
