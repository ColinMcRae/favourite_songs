class Song < ActiveRecord::Base
  validates :url, format: URI.regexp(%w[http https]), allow_blank: true
  validates_presence_of :author, :name, :list_id
  belongs_to :list
end
