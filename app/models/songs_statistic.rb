class SongsStatistic < ApplicationRecord
  belongs_to :song
  validates_presence_of :song_id

  class << self
    def collect(song_id)
      return unless song_id
      where(song_id: song_id, created_at: Time.now.beginning_of_day..Time.now.end_of_day)
        .first_or_create.increment!(:count)
    end

    def top_genre
      SongsStatistic.includes(:song).joins(:song)
                    .where('songs_statistics.created_at > ?', (Time.now - 1.month).beginning_of_day)
                    .group('songs.genre').sum('songs_statistics.count')
    end
  end
end
