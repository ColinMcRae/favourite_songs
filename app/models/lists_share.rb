class ListsShare < ActiveRecord::Base
  validates_uniqueness_of :user_id, scope: :list_id
  belongs_to :user
  belongs_to :list
end
