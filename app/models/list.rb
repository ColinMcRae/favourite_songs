class List < ActiveRecord::Base
  validates_presence_of :name, :user_id
  has_many :songs
  belongs_to :user

  scope :public_lists, -> { where(is_public: true) }
end
