Rails.application.routes.draw do
  devise_for :users

  root to: 'dashboard#index'

  resources :lists, only: [:show, :create, :update, :destroy]
  post '/share_list/:id', to: 'lists#share', as: 'share_list'
  get 'my_lists', to: 'lists#index'

  resources :songs, only: [:update, :create, :destroy]
  post 'collect_click', to: 'songs_statistic#collect', format: :js

  namespace :api, defaults: {format: :json} do
    devise_for :users
    resources :songs, only: [:update, :create, :destroy]
    resources :lists, only: [:index, :show, :create, :update, :destroy]
    get 'dashboard', to: 'dashboard#index'
  end

end
