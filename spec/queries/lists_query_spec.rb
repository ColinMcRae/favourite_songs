require 'rails_helper'

describe ListsQuery do
  describe '#all' do
    let(:users) { create_list :user, 3 }

    let!(:list1) { create :list, user_id: users[0].id }
    let!(:list2) { create :list, user_id: users[1].id }
    let!(:list3) { create :list, user_id: users[2].id }

    subject do
      described_class.new.all(users[0].id)
    end

    context 'when user has shared lists' do
      before { ListsShare.create(user_id: users[0].id, list_id: list2.id) }
      it 'return own and shared lists' do
        expect(subject).to eq [list1, list2]
      end
    end

    context 'when user has only own list' do
      it 'return only own list' do
        expect(subject).to eq [list1]
      end
    end

    context 'when other user has public list' do
      let!(:list4) { create :list, user_id: users[2].id, is_public: true }

      it 'returns own and public lists' do
        expect(subject).to eq [list1, list4]
      end
    end
  end
end
