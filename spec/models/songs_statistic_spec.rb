require 'rails_helper'

describe SongsStatistic do
  let(:user) { create :user }
  let(:list) { create :list, user: user }
  let(:song) { create :song, list: list }

  describe '#collect' do
    context 'creates new record when statiscics not found' do
      before { SongsStatistic.collect(song.id) }
      it { expect(SongsStatistic.count).to eq(1) }
    end

    context 'increments count when statistics exists' do
      let!(:stat) { SongsStatistic.create!(song_id: song.id, count: 4) }

      before { SongsStatistic.collect(song.id) }

      it { expect(SongsStatistic.count).to eq(1) }
      it { expect(stat.reload.count).to eq(5) }
    end

    context 'creates new record for next day' do
      let!(:stat) { SongsStatistic.create!(song_id: song.id, count: 4, created_at: Time.now.beginning_of_day - 1.hour) }
      before { SongsStatistic.collect(song.id) }

      it { expect(SongsStatistic.count).to eq(2) }
      it { expect(stat.reload.count).to eq(4) }
    end
  end

  describe '#top_genre' do
    let(:song1) { create :song, list: list, genre: 'g1' }
    let(:song2) { create :song, list: list, genre: 'g2' }
    let(:song3) { create :song, list: list, genre: 'g3' }
    let(:song4) { create :song, list: list, genre: 'g3' }
    let(:song5) { create :song, list: list, genre: 'g1' }
    before do
      SongsStatistic.create song_id: song1.id, count: 5
      SongsStatistic.create song_id: song1.id, count: 2, created_at: Time.now - 36.days
      SongsStatistic.create song_id: song5.id, count: 1

      SongsStatistic.create song_id: song2.id, count: 3

      SongsStatistic.create song_id: song3.id, count: 1
      SongsStatistic.create song_id: song4.id, count: 1, created_at: Time.now - 36.days
    end

    context 'gets top genres for last month' do
      it { expect(SongsStatistic.top_genre).to eq('g1' => 6, 'g2' => 3, 'g3' => 1) }
    end
  end
end
