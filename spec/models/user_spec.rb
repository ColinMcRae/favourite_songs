require 'rails_helper'

describe User do
  context 'valid' do
    it('with all fields') { expect(build(:user)).to be_valid }
    it('without info') { expect(build(:user, info: nil)).to be_valid }
  end

  context 'not valid' do
    it('without password') { expect(build(:user, password: nil)).to_not be_valid }
    it('without username') { expect(build(:user, username: nil)).to_not be_valid }
    it('without email') { expect(build(:user, email: nil)).to_not be_valid }
  end
end
