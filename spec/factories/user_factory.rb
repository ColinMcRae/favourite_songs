FactoryBot.define do
  factory :user do
    sequence(:username) { Faker::Internet.user_name }
    sequence(:email) { Faker::Internet.email }
    sequence(:password) { Faker::Internet.password }
    sequence(:info) { Faker::Lorem.sentence }
  end
end
