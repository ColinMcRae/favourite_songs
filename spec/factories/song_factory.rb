FactoryBot.define do
  factory :song do
    sequence(:author) { Faker::Name.name }
    sequence(:name) { Faker::Lorem.word }
    genre Faker::Lorem.word
    url Faker::Internet.url
  end
end
