FactoryBot.define do
  factory :list do
    sequence(:name) { Faker::Lorem.word }
    sequence(:description) { Faker::Lorem.sentence }
    is_public false
  end
end
