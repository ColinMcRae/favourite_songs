require 'rails_helper'
include Pundit::RSpec::Matchers
include Pundit::RSpec::DSL

describe ListPolicy do
  subject { described_class }
  let(:user2) { create :user }
  let(:user) { create :user }
  let(:user3) { create :user }
  let(:mylist) { create :list, user: user }
  let(:public_list) { create :list, user: user, is_public: true }
  let(:shared_list) { create :list, user: user2 }
  let(:mysong) { create :song, list: mylist }
  let(:list2) { create :list, user: user2 }
  let!(:lists_share) { create :lists_share, user: user, list: shared_list }

  permissions :index? do
    it 'accepted for authorized user' do
      expect(subject).to permit(user, mylist)
    end
  end

  permissions :update?, :destroy?, :share? do
    it 'accepted for owner' do
      expect(subject).to permit(user, mylist)
    end

    it 'denied for other user' do
      expect(subject).to_not permit(user2, mylist)
    end
  end

  permissions :show? do
    it 'accepted for owner' do
      expect(subject).to permit(user, mylist)
    end

    it 'accepted for shared list' do
      expect(subject).to permit(user, shared_list)
    end

    it 'accepted for public list' do
      expect(subject).to permit(user, public_list)
    end

    it 'denied for foreign list' do
      expect(subject).to_not permit(user3, mylist)
    end

    it 'denied for shared foreigh list' do
      expect(subject).to_not permit(user3, shared_list)
    end
  end
end
