require 'rails_helper'
include Pundit::RSpec::Matchers
include Pundit::RSpec::DSL

describe SongPolicy do
  subject { described_class }
  let(:user2) { create :user }
  let(:user) { create :user }
  let(:mylist) { create :list, user: user }

  let(:mysong) { create :song, list: mylist }

  permissions :update?, :destroy? do
    it 'accepted for owner' do
      expect(subject).to permit(user, mysong)
    end

    it 'denied for other user' do
      expect(subject).to_not permit(user2, mysong)
    end
  end
end
