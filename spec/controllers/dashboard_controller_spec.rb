require 'rails_helper'

context DashboardController do
  describe '#index' do
    context 'unsigned' do
      before { get :index }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context 'signed in' do
      let(:user) { create :user }
      before do
        sign_in user
      end

      context 'can access page' do
        before { get :index }
        it { expect(response).to have_http_status(:success) }
      end

      context 'can see own and shared lists' do
        let!(:user2) { create :user }
        let!(:user3) { create :user }
        let!(:list1) { create :list, user_id: user.id }
        let!(:list2_shared) { create :list, user_id: user2.id }
        let!(:list2_private) { create :list, user_id: user2.id }
        let!(:list3_public) { create :list, user_id: user3.id, is_public: true }
        before do
          ListsShare.create(list_id: list2_shared.id, user_id: user.id)
          get :index
        end

        it { expect(assigns(:lists)).to match_array([list1, list2_shared, list3_public]) }
      end

      context 'load statistics'
    end
  end
end
