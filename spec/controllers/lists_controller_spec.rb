require 'rails_helper'

context ListsController, type: :controller do
  let(:user) { create :user }
  let!(:list) { create :list, user_id: user.id }
  let(:song1) { create :song, list_id: list.id }
  let(:song2) { create :song, list_id: list.id }
  let(:list_params) { { list: { name: 'listname', user_id: user.id } } }
  let(:invalid_list_params) { { list: { name: nil, user_id: user.id } } }

  describe 'unsigned user' do
    context '#index' do
      before { get :index }
      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context '#create' do
      before { post :create, params: list_params }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end

    context '#show' do
      before { get :show, params: { id: list.id } }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end

  describe 'signed in user' do
    before do
      sign_in user
    end

    context '#index' do
      before { get :index }
      it { expect(response).to have_http_status(:success) }
      it { expect(assigns(:lists)).to eq([list]) }
      it { expect(assigns(:new_list)).to be_new_record }
    end

    context '#create' do
      it { expect(post(:create, params: list_params)).to have_http_status(:redirect) }
      it { expect { post :create, params: list_params }.to change(List, :count).by(1) }
      it { expect { post :create, params: invalid_list_params }.to change(List, :count).by(0) }

      context 'set success flash with valid params' do
        before { post :create, params: list_params }
        it { expect(expect(flash[:success])).to be_present }
      end

      context 'set error flash with invalid params' do
        before { post :create, params: invalid_list_params }
        it { expect(expect(flash[:danger])).to be_present }
      end
    end

    context '#show' do
      before { get :show, params: { id: list.id } }
      it { expect(response).to have_http_status(:success) }
      it { expect(assigns(:list)).to eq(list) }
      it { expect(assigns(:new_song)).to be_new_record }
      it { expect(assigns(:songs)).to eq([song1, song2]) }
    end

    context '#update' do
      let(:edit_list_params) { { name: 'newname' } }
      before { patch :update, params: { id: list.id, list: edit_list_params } }
      it { expect(list.reload.name).to eq(edit_list_params[:name]) }
    end

    context '#destroy' do
      it { expect { delete :destroy, params: { id: list.id } }.to change(List, :count).by(-1) }
    end

    context '#share' do
      let!(:user2) { create :user }
      let!(:user3) { create :user }
      let!(:list1) { create :list, user_id: user.id }
      let!(:list2) { create :list, user_id: user2.id }
      let!(:list2_1) { create :list, user_id: user2.id }
      let!(:list3_public) { create :list, user_id: user3.id, is_public: true }
      let(:share_params) { { user2.id => '' } }
      let(:two_users_share) { { user3.id => '', user2.id => '' } }
      let(:share_params_u3) { { user3.id => '' } }

      context 'without previous shares' do
        it 'creates one new share' do
          expect { post :share, params: { id: list.id, share: share_params } }.to change(ListsShare, :count).by(1)
        end
      end

      context 'when shared with one user' do
        before { ListsShare.create(user_id: user2.id, list_id: list.id) }

        it 'dont create share when already shared with user' do
          expect { post :share, params: { id: list.id, share: share_params } }.to change(ListsShare, :count).by(0)
        end

        it 'creates new share when shared with another user' do
          expect { post :share, params: { id: list.id, share: two_users_share } }.to change(ListsShare, :count).by(1)
        end

        it 'removes share with user' do
          post :share, params: { id: list.id, share: share_params_u3 }

          expect(ListsShare.pluck(:user_id)).to_not include(user2.id)
          expect(ListsShare.pluck(:user_id)).to include(user3.id)
        end
      end
    end
  end
end
