require 'rails_helper'

context SongsStatisticController, type: :controller do
  let(:user) { create :user }
  let(:list) { create :list, user: user }
  let(:song) { create :song, list: list }
  let(:click) { { song_id: song.id } }

  describe '#collect' do
    before { expect(SongsStatistic).to receive(:collect).once }

    context 'unsigned user clicks' do
      before { post :collect, params: click, format: 'js', xhr: true }
      it { expect(response).to have_http_status(:success) }
    end

    context 'signed_in user clicks' do
      before { post :collect, params: click, xhr: true }
      it { expect(response).to have_http_status(:success) }
    end
  end
end
