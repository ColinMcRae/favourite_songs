require 'rails_helper'

context SongsController, type: :controller do
  let(:user) { create :user }
  let(:list) { create :list, user_id: user.id }
  let!(:song) { create :song, list_id: list.id }
  let(:song_params) { { song: { name: 'newsong', author: 'author', list_id: list.id } } }
  let(:invalid_song_params) { { song: { name: nil, author: 'author', list_id: list.id } } }

  describe 'unsigned user' do
    context '#create' do
      before { post :create, params: song_params }

      it { expect(response).to have_http_status(:redirect) }
      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end

  describe 'signed in user' do
    before do
      sign_in user
    end

    context '#create' do
      it { expect(post(:create, params: song_params)).to redirect_to list_path(id: list.id) }
      it { expect { post :create, params: song_params }.to change(Song, :count).by(1) }
      it { expect { post :create, params: invalid_song_params }.to change(Song, :count).by(0) }

      context 'set success flash with valid params' do
        before { post :create, params: song_params }
        it { expect(expect(flash[:success])).to be_present }
      end

      context 'set error flash with invalid params' do
        before { post :create, params: invalid_song_params }
        it { expect(expect(flash[:danger])).to be_present }
      end
    end

    context '#update' do
      let(:edit_song_params) { { name: 'newname' } }
      before { patch :update, params: { id: song.id, song: edit_song_params } }
      it { expect(song.reload.name).to eq(edit_song_params[:name]) }
    end

    context '#destroy' do
      it { expect { delete :destroy, params: { id: song.id } }.to change(Song, :count).by(-1) }
    end
  end
end
