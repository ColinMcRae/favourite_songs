require 'rails_helper'

describe 'Api::SongsController' do
  describe 'authorized' do
    let(:token) { Faker::Lorem.characters(20) }
    let(:user) { create :user, remember_token: token }
    let(:list) { create :list, user_id: user.id }
    let!(:song) { create :song, list_id: list.id }
    let(:song_params) { { song: { name: 'newsong', author: 'author', list_id: list.id }, auth_token: token } }

    context '#create' do
      it { expect { post '/api/songs', params: song_params }.to change(Song, :count).by(1) }
      it 'contains status: success in responce' do
        post '/api/songs', params: song_params
        expect(JSON.parse(response.body)).to include('status' => 'success')
      end
    end

    context '#update' do
      let(:edit_song_params) { { name: 'newname' } }

      before { patch "/api/songs/#{song.id}", params: { song: edit_song_params, auth_token: token } }
      it { expect(song.reload.name).to eq(edit_song_params[:name]) }
    end

    context '#destroy' do
      it { expect { delete "/api/songs/#{song.id}", params: { auth_token: token } }.to change(Song, :count).by(-1) }
    end
  end
end
