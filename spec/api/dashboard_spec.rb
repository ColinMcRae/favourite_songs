require 'rails_helper'

describe 'Api::DashboardController' do
  describe '#index' do
    let(:token) { Faker::Lorem.characters(20) }
    let!(:user) { create :user, remember_token: token }

    context 'unsigned' do
      context 'can not access page' do
        before { get '/api/dashboard' }

        it { expect(response).to have_http_status(:unauthorized) }
        it { expect(JSON.parse(response.body)).to eq('errors' => 'Access denied') }
      end
    end

    context 'signed in user' do
      let!(:user2) { create :user }
      let!(:user3) { create :user }
      let!(:list1) { create :list, user_id: user.id }
      let!(:list2_shared) { create :list, user_id: user2.id }
      let!(:list2_private) { create :list, user_id: user2.id }
      let!(:list3_public) { create :list, user_id: user3.id, is_public: true }

      before do
        ListsShare.create(list_id: list2_shared.id, user_id: user.id)
        get "/api/dashboard?auth_token=#{token}"
      end

      it { expect(response).to have_http_status(:success) }
      it 'can see lists' do
        list_ids = JSON.parse(response.body)['lists'].map { |l| l['id'] }
        expect(list_ids).to match_array([list1.id, list2_shared.id, list3_public.id])
      end
    end
  end
end
