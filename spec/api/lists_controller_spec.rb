require 'rails_helper'

describe 'Api::ListsController' do
  describe 'unauthorized' do
    context '#index' do
      before { get '/api/lists' }
      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'user signed in' do
    let(:token) { Faker::Lorem.characters(20) }
    let(:user) { create :user, remember_token: token }
    let!(:list) { create :list, user_id: user.id }
    let!(:song1) { create :song, list_id: list.id }
    let!(:song2) { create :song, list_id: list.id }
    let(:list_params) { { list: { name: 'listname', user_id: user.id }, auth_token: token } }

    context '#index' do
      before { get "/api/lists?auth_token=#{token}" }
      it { expect(response).to have_http_status(:success) }
      it 'json contains ' do
        json = JSON.parse response.body
        expect(json['lists'][0]['id']).to eq(list.id)
        expect(json['user_id']).to eq(user.id)
      end
    end

    context '#create' do
      it { expect { post('/api/lists', params: list_params) }.to change(List, :count).by(1) }
      it 'contains status: success in responce' do
        post('/api/lists', params: list_params)
        expect(JSON.parse(response.body)).to include('status' => 'success')
      end
    end

    context '#show' do
      before { get "/api/lists/#{list.id}", params: { auth_token: token } }
      it { expect(response).to have_http_status(:success) }
      it 'json contains list and songs' do
        json = JSON.parse response.body
        expect(json['list']['id']).to eq(list.id)
        expect(json['songs'].map { |s| s['id'] }).to eq([song1.id, song2.id])
        expect(json['user_id']).to eq(user.id)
      end
    end

    context '#update' do
      let(:edit_list_params) { { name: 'newname' } }
      before { patch "/api/lists/#{list.id}", params: { list: edit_list_params, auth_token: token } }
      it { expect(list.reload.name).to eq(edit_list_params[:name]) }
    end

    context '#destroy' do
      it { expect { delete :"/api/lists/#{list.id}", params: { auth_token: token } }.to change(List, :count).by(-1) }
    end
  end
end
